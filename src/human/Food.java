package human;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation type for Food.
 *
 * <p>
 *     Contains name of the food and the calorie.
 *     Used by Human class. If eat method invoked with no arguments,
 *     then we use Annotation value. In standard case user enters
 *     the specific argument about his/her food.
 * </p>
 *
 * @author Muhammed Bera KOÇ
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Food {
    String name() default "Cake";
    double calorie() default 352.8;
}
