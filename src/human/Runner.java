package human;

public class Runner {

    public static void main(String[] args) {
        Human richard = new Human("Richard", 28);
        richard.eat();
        Human edward = new Human("Edward", 95);
        edward.eat("Egg", 120.2);
    }
}
