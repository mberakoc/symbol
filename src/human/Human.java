package human;

import java.lang.reflect.Method;

/**
 * Human is a class to demonstrate how to use annotations for methods
 *
 * @author Muhammed Bera KOÇ
 */
public class Human {

    String name;
    int age;

    /**
     * Constructor method for Human class.
     *
     * @param name Indicates name of the human being.
     * @param age Indicates age of the human being.
     */
    public Human(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * A method to conduct eating by human.
     *
     * @param food The name of the food to be eaten
     * @param calorie The calorie of the food to be eaten
     */
    public void eat(String food, double calorie) {
        System.out.println(name + " is " + age + " years old." +
                " He is eating " + food + " which contains " + calorie + " cal.");
    }

    /**
     * Overloaded version of the first eat method.
     * <p>
     *     When no args given this one will be invoked. The method uses annotation
     *     to obtain info about the food unlike the first one.
     * </p>
     *
     */
    @Food(name = "Beacon", calorie = 459.2)
    public void eat() {
        Class<?> humanClass = getClass();
        try {
            Method method = humanClass.getMethod("eat");
            Food food = method.getAnnotation(Food.class);
            System.out.println(name + " is " + age + " years old." +
                    " He is eating " + food.name() + " which contains " + food.calorie() + " cal.");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
