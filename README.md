# [Symbol](https://www.wikizeroo.org/index.php?q=aHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvTmV1cm8tbGluZ3Vpc3RpY19wcm9ncmFtbWluZw) <br>[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-yellow.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Generic badge](https://img.shields.io/badge/version-v1.0.1-brightgreen.svg)](https://shields.io/) [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-blueviolet.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
A mini-project which contains some useful implementation of Annotations in Java
## Licence 
Symbol is [GNU Licensed.](https://gitlab.com/mberakoc/symbol/blob/master/LICENSE)
